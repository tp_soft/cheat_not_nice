package com.twise.Seeder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;

/**
 * Created by omitobisam on 8.05.16.
 */
public class CompanySeed {

    String [] company = new String[7];

    public String[] toCompany(String fileName){
        String[] company1 = new String[7];
        JSONParser parser = new JSONParser();
        int num = 0;

        try {
            JSONObject obj = (JSONObject) parser.parse(new FileReader(fileName));
            String comp = (String) obj.get("companies");
            String ncomp = comp.substring(comp.indexOf('[')+1, comp.indexOf(']'));
            String [] ncomp2 = ncomp.split(", ");

            for (String c: ncomp2){
                company1[num] = c;
                num++;
            }




        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return company1;
    }

    public String[] getCompany() {
        return company;
    }

    public void setCompany() {
        this.company = toCompany("data/company.json");
    }
}
