package com.twise.Seeder;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.apache.http.protocol.HTTP.USER_AGENT;

/**
 * Created by omitobisam on 8.05.16.
 */
public class SeedMain {

    String[][] allDataSource = new String[20][2];

    public void doData(String url, String loc) throws Exception {
        InputStream input = null;
        OutputStream output = null;
        try {
            input = new URL(url).openStream();
            output = new FileOutputStream(loc);
            byte[] buffer = new byte[1024];
            for (int length = 0; (length = input.read(buffer)) > 0;) {
                output.write(buffer, 0, length);
            }
            // Here you could append further stuff to `output` if necessary.
        } finally {
            if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
            if (input != null) try { input.close(); } catch (IOException logOrIgnore) {}
        }
    }

    public void postMaker(String url)throws Exception{
        //String url = "http://bootcamp-api.transferwise.com/task/3/start?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("User-Agent", USER_AGENT);

        //post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);
        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        System.out.println(result);
    }




    public void getMaker(String url) throws Exception{
        //url = "http://bootcamp-api.transferwise.com/task/3?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);

        // add request header
        request.addHeader("User-Agent", USER_AGENT);
        HttpResponse response = client.execute(request);

        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result);
    }

    public void makeURL(String method, String amount){
        CurrencySeed currencySeed = new CurrencySeed();
        currencySeed.setCurrency();
        String[]currency = currencySeed.getCurrency();
        String[] quoteUrl = new String[20];
        String[] rateUrl = new String[20];
        int num = 0;
        if(method.equals("GET")){
            for (String curr: currency
                 ) {
                for (String curr2:currency
                     ) {
                    if (curr.equals(curr2)){
                        continue;
                    }else {
                        quoteUrl[num] = "http://bootcamp-api.transferwise.com/quote/"+amount+"/"+curr+"/"+curr2+"?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";
                        String dataSrc = "data/quote/"+curr+curr2+".json";
                        //try {doData(quoteUrl[num], dataSrc);  } catch (Exception e){e.printStackTrace();}
                        this.allDataSource[num][0]=dataSrc;
                        System.out.println(dataSrc);
                        rateUrl[num] = "http://bootcamp-api.transferwise.com/rate/midMarket"+"/"+curr+"/"+curr2+"?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";
                        dataSrc = "data/rate/"+curr+curr2+".json";
                        try {doData(rateUrl[num],dataSrc ); } catch (Exception e){e.printStackTrace();}
                        this.allDataSource[num][1]=dataSrc;
                        num++;
                    }
                }
            }
        }else if(method.equals("POST")){

        }
        int n = 0;
        for (String u: quoteUrl) {
            /*System.out.println(u);
            System.out.println(rateUrl[n]);*/
            n++;
        }
    }

    public String[][] getAllDataSource() {
        return allDataSource;
    }
}
