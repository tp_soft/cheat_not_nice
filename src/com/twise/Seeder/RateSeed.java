package com.twise.Seeder;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by omitobisam on 8.05.16.
 */
public class RateSeed {

    String rate [][] = new String[20][3];
    public String[][] toRate(String fileName){
        String rate1 [][] = new String[20][3];
        JSONParser parser = new JSONParser();
        int num = 0;

        CurrencySeed currencySeed = new CurrencySeed();
        currencySeed.setCurrency();
        String[]currency = currencySeed.getCurrency();

            for (String curr : currency
                    ) {
                for (String curr2 : currency
                        ) {
                    if (curr.equals(curr2)) {
                        continue;
                    } else {
                        try {
                            JSONObject obj = (JSONObject) parser.parse(new FileReader(fileName));
                            String rt = Double.toString((Double) obj.get("rate"));
                            rate1[num][0] = curr;
                            rate1[num][1] = curr2;
                            rate1[num][2] = rt;
                            num++;

                            //System.out.println(rate1);
                        } catch (FileNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }



        return rate1;
    }

    public String[][] getRate() {
        return rate;
    }

    public void setRate(String rateFileName) {
        this.rate = toRate(rateFileName);
    }

}
