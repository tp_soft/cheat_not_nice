package com.twise.Seeder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.google.gson.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by omitobisam on 8.05.16.
 */
public class QuoteSeed {


    String [][] quote;

    public String[][] toQuote(String fileName){

        CompanySeed companyseed = new CompanySeed();
        String[][] quote = new String[7][5];

        String[] company;

        companyseed.setCompany();
        company = companyseed.getCompany();

        JSONParser parser = new JSONParser();
        int num = 0;

        try {
            JSONObject obj = (JSONObject)parser.parse(new FileReader(fileName));

            for (String cmp: company
                 ) {
                JSONObject cmp1 = (JSONObject)obj.get(cmp);
                //System.out.println(cmp1);
                String rrc = Double.toString((Double)cmp1.get("recipientReceives"));
                String srcCurr = (String)cmp1.get("sourceCurrency");
                String tarCurr = (String)cmp1.get("targetCurrency");
                String amnt = Long.toString((Long) cmp1.get("sourceAmount"));
                quote[num][0] = cmp;
                quote[num][1] = rrc;
                quote[num][2] = srcCurr;
                quote[num][3] = tarCurr;
                quote[num][4] = amnt;
//                System.out.println(cmp+" "+rrc);
                num++;
            }

           /* for (Object a: aa
                 ) {
                System.out.println(a);
            }*/

/*            for (Object o : array)
            {
                JSONObject acc = (JSONObject) o;
                quote[num][0] = (String)o;
                num++;
            }*/

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return quote;

    }

    public String[][] getQuote() {
        return quote;
    }

    public void setQuote(String quoteFileName) {
        this.quote = toQuote(quoteFileName);
    }


}
