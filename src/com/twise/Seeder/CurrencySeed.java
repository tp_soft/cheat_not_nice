package com.twise.Seeder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by omitobisam on 8.05.16.
 */
public class CurrencySeed {

    String [] currency  = new String[5];

    public String[] toCurrency(String fileName) {
        String[] currency1 = new String[5];
        JSONParser parser = new JSONParser();
        int num = 0;

        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(fileName));

            for (Object o : a)
            {
                currency1[num] = (String)o;
                num++;
            }


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return currency1;
    }

    public String[] getCurrency() {
        return currency;
    }

    public void setCurrency() {
        this.currency = toCurrency("data/currency.json");
    }
}
