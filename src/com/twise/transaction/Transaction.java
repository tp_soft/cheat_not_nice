
package com.twise.transaction;

import com.twise.Seeder.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static org.apache.http.protocol.HTTP.USER_AGENT;

/**
 * Created by omitobisam on 8.05.16.
 */
public class Transaction {
    public void getPayments(String amount){
        CompanySeed companySeed = new CompanySeed();
        companySeed.setCompany();
        String company[] = companySeed.getCompany();
        //
        CurrencySeed currencySeed = new CurrencySeed();
        currencySeed.setCurrency();
        String currency[] = currencySeed.getCurrency();

        //Make Quote and Rate

        SeedMain seedMain = new SeedMain();
        seedMain.makeURL("GET", amount);
        String [][]dataSource = seedMain.getAllDataSource();

        //Ends here

        QuoteSeed quoteSeed = new QuoteSeed();
        String quote[][] = new String[7][5];

        RateSeed rateSeed = new RateSeed();
        double rts[][] = new double[20][3];

        String [][] theQuoteData = new String[7][5];

        int l = 0;
        for (String[] dss: dataSource) {
            for(String d: dss) {

                if(d.contains("quote")) {
                    quoteSeed.setQuote(d);
                    quote = quoteSeed.getQuote();
                    //System.out.println(quote);
                    int n =0;
                    for (String[]qs: quote
                         ) {
                        theQuoteData[n][0] = quote [n][0];
                        theQuoteData[n][1] = quote [n][1];
                        theQuoteData[n][2] = quote [n][2];
                        theQuoteData[n][4] = quote [n][4];
                        theQuoteData[n][3] = quote [n][3];

                       // System.out.println(quote [n][0]+quote [n][1]+quote [n][2]+quote [n][3]);
                        n++;
                    }

                }
                int m = 0;
                if(d.contains("rate")) {
                    rateSeed.setRate(d);
                    String nRate[][] = rateSeed.getRate();
                    String srcN = nRate[l][0];
                    String desN = nRate[l][1];
                    for (String cpy[]:theQuoteData
                         ) {


                        String srcCur = theQuoteData[m][2];
                        String destQ = theQuoteData[m][3];
                        for (String cmpn: company
                             ) {
                            String cmpy = theQuoteData[m][0];
                            if (cmpn.equals(cmpy)){

                                String amnt = String.valueOf(Double.valueOf(nRate[l][2]) * Double.valueOf(amount));
                                //System.out.println(amnt + cmpy);
                                String amntQ = theQuoteData[m][1];
                                //System.out.println(cmpy + amntQ + theQuoteData[m][2] + theQuoteData[m][3] + amnt);

                                if(srcCur.equals(srcN)) {
                                    String newRate = nRate[l][2];
                                    String nTotalSrcQ = String.valueOf(Double.valueOf(newRate)*Double.valueOf(amntQ));
                                    String nTotalSrcN = String.valueOf(Double.valueOf(newRate)*Double.valueOf(amnt));
                                    String diff = String.valueOf(Double.valueOf(nTotalSrcN) - Double.valueOf(nTotalSrcQ));
                                    //System.out.println(diff);
                                    String perc = String.valueOf((Double.valueOf(diff) / Double.valueOf(nTotalSrcN)) * 100.0);
                                    //System.out.println(perc);
                                    String strUrl = "http://bootcamp-api.transferwise.com/hiddenFee/forCompany/" + cmpy + "/" + amount + "/" + srcCur + "/" + theQuoteData[m][2] + "/" + perc +
                                            "?token=c9c1cd7c79105212ad5b4978e5e4c2d3a45cb00a";
                                    //System.out.println(perc);
                                    try {
                                        seedMain.postMaker(strUrl);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }

                        // System.out.println(amnt + "\t" + amntQ + "\t="+diff );
                        //System.out.println(rts[m]);
                        //System.out.println(m);
                        m++;
                    }
                    System.out.println(l);
                    l++;


                    //System.out.println(rates);

                }

            }
        }

        //Quote: Companyname RecipientRecieves SourceCurrency TargetCurrency Amount

        for (String[] qs:quote
             ) {
            for (String q:qs
                 ) {
                //System.out.println(q);
            }
        }



       // System.out.println(rates);

    }

}
